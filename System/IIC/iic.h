#ifndef __IIC_H
#define __IIC_H
#include "stm32f4xx.h"

void IIC_GPIO_Init(void);
void Start_IIC(void);
void Stop_IIC(void);
void IIC_Send_Byte(uint8_t Byte);
uint8_t IIC_Receive_Byte(void);
void IIC_Send_Ack(uint8_t AckBit);
uint8_t IIC_Receive_Ack(void);

#endif
