/*
*编写者：zhaoyue
*编写日期：2023年3月31日
*使用说明：软件配置IO模拟IIC，只需要配置相关GPIO引脚即可
*其他说明：根据B站博主(江科大自化协)的教程编写，感谢大佬的贡献！！！
*/

#include "iic.h"
#include "delay.h"
/*
*函数功能：初始化IIC端口，配置单片机GPIO引脚
*传入参数：无
*返回参数：无
*SCL --> (GPIOA,GPIO_Pin_3)
*SDA --> (GPIOA,GPIO_Pin_5)
*/
void IIC_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_5;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_SetBits(GPIOA, GPIO_Pin_3 | GPIO_Pin_5);
}

/*
*函数功能：IIC引脚定义
*传入参数：BitValue
*返回参数：无
*/
void IIC_Write_SCL(uint8_t BitValue)
{
	GPIO_WriteBit(GPIOF ,GPIO_Pin_5 ,(BitAction)BitValue);
	delay_us(10);
}

/*
*IIC引脚定义
*传入参数：BitValue
*返回参数：无
*/
void IIC_Write_SDA(uint8_t BitValue)
{
	GPIO_WriteBit(GPIOF ,GPIO_Pin_6 ,(BitAction)BitValue);
	delay_us(10);
}

/*
*IIC引脚定义
*传入参数：无
*返回参数：BitValue
*/
uint8_t IIC_Read_SDA(void)
{
	uint8_t BitValue;
	BitValue = GPIO_ReadInputDataBit(GPIOF ,GPIO_Pin_6);
	delay_us(10);
	return BitValue;
}

/*
*开启IIC
*传入参数：无
*返回参数：无
*/
void Start_IIC(void)
{
	IIC_Write_SDA(1);
	IIC_Write_SCL(1);
	IIC_Write_SDA(0);
	IIC_Write_SCL(0);
}

/*
*函数功能：停止IIC
*传入参数：无
*返回参数：无
*/
void Stop_IIC(void)
{
	
	IIC_Write_SDA(0);
	IIC_Write_SCL(1);
	IIC_Write_SDA(1);
}

/*
*函数功能：IIC发送一个字节
*传入参数：Byte
*返回参数：无
*/
void IIC_Send_Byte(uint8_t Byte)
{
	uint8_t i;
	for(i = 0;i < 8;i++)
	{
		IIC_Write_SDA(Byte&(0x80>>i));
		IIC_Write_SCL(1);
		IIC_Write_SCL(0);
	}

}

/*
*函数功能：IIC接收一个字节
*传入参数：无
*返回参数：Byte
*/
uint8_t IIC_Receive_Byte(void)
{
	uint8_t i,Byte = 0x00;
	IIC_Write_SDA(1);
	for(i = 0;i < 8;i++)
	{
		IIC_Write_SCL(1);
		if(IIC_Read_SDA() == 1){Byte |= (0x80>>i);}
		IIC_Write_SCL(0);
	}      
	return Byte;
}

/*
*函数功能：IIC发送应答
*传入参数：AckBit
*返回参数：无
*/
void IIC_Send_Ack(uint8_t AckBit)
{
	IIC_Write_SDA(AckBit);
	IIC_Write_SCL(1);
	IIC_Write_SCL(0);
}

/*
*函数功能：IIC接收应答
*传入参数：无
*返回参数：AckBit
*/
uint8_t IIC_Receive_Ack(void)
{
	uint8_t AckBit;
	IIC_Write_SDA(1);
	IIC_Write_SCL(1);
	AckBit = IIC_Read_SDA();
	IIC_Write_SCL(0);
	return AckBit;
}

