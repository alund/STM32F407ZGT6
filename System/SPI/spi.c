/*
*编写者：zhaoyue
*编写日期：2023年4月1日
*使用说明：软件配置IO模拟SPI，只需要配置相关GPIO引脚即可
*其他说明：根据B站博主(江科大自化协)的教程编写，感谢大佬的贡献！！！
*/

#include "spi.h"
/**
*函数功能：
*传入参数：
*返回参数：
*/
void Write_SPI_CS(uint8_t BitValue)
{
	GPIO_WriteBit(GPIOA, GPIO_Pin_4, (BitAction)BitValue);
}
/**
*函数功能：
*传入参数：
*返回参数：
*/
void Write_SPI_SCK(uint8_t BitValue)
{
	GPIO_WriteBit(GPIOA, GPIO_Pin_5, (BitAction)BitValue);
}
/**
*函数功能：
*传入参数：
*返回参数：
*/
uint8_t Read_SPI_MISO(void)
{
	return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6);
}
/**
*函数功能：
*传入参数：
*返回参数：
*/
void Write_SPI_MOSI(uint8_t BitValue)
{
	GPIO_WriteBit(GPIOA, GPIO_Pin_7, (BitAction)BitValue);
}

/**
*函数功能：初始化SPI相关的引脚
*传入参数：无
*返回参数：无
*/
void SPI_init(void)
{
	
	Write_SPI_CS(1);
	Write_SPI_SCK(0);
}

/**
*函数功能：启动SPI
*传入参数：无
*返回参数：无
*/
void Start_SPI(void)
{
	Write_SPI_CS(0);
}

/**
*函数功能：停止SPI
*传入参数：无
*返回参数：无
*/
void Stop_SPI(void)
{
	Write_SPI_CS(1);
}

/**
*函数功能：收发一个字节的数据
*传入参数：SendByte
*返回参数：ReceiveByte
*/
uint8_t SPI_SwapByte(uint8_t SendByte)
{
	uint8_t i, ReceiveByte = 0x00;
	
	for (i = 0; i < 8; i ++)
	{
		Write_SPI_MOSI(SendByte & (0x80 >> i));
		Write_SPI_SCK(1);
		if (Read_SPI_MISO() == 1){ReceiveByte |= (0x80 >> i);}
		Write_SPI_SCK(0);
	}
	
	return ReceiveByte;
}

