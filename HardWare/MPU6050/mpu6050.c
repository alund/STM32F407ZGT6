/*
*编写者：zhaoyue
*编写日期：2023年3月31日
*使用说明：IIC和mpu6050建立通信后读取传感器的数据，判断mpu6050的位姿
*其他说明：根据B站博主(江科大自化协)的教程编写,感谢大佬的贡献！！！
*/

#include "mpu6050.h"

/*
*函数功能：读取MPU6050寄存器中的数据
*传入参数：RegisterAddress，Data
*返回参数：无
*/
void Write_MPU6050_Register(uint8_t RegisterAddress ,uint8_t Data)
{
	Start_IIC();
	IIC_Send_Byte(MPU6050_Address);
	IIC_Receive_Ack();
	
	IIC_Send_Byte(RegisterAddress);
	IIC_Receive_Ack();
	
	IIC_Send_Byte(Data);
	IIC_Receive_Ack();
	
	Stop_IIC();
}

/*
*函数功能：读取MPU6050寄存器中的数据
*传入参数：RegisterAddress
*返回参数：Data
*/
uint8_t Read_MPU6050_Register(uint8_t RegisterAddress)
{
	uint8_t Data;
	Start_IIC();
	IIC_Send_Byte(MPU6050_Address);
	IIC_Receive_Ack();
	
	IIC_Send_Byte(RegisterAddress);
	IIC_Receive_Ack();
	
	Start_IIC();
	IIC_Send_Byte(MPU6050_Address | 0x01);
	IIC_Receive_Ack();
	
	Data = IIC_Receive_Byte();
	IIC_Send_Ack(1);
	
	Stop_IIC();
	return Data;
}

/*
*函数功能：初始化MPU6050配置
*传入参数：无
*返回参数：无
*/
void MPU6050_Init(void)
{
	IIC_GPIO_Init();
	Write_MPU6050_Register(MPU6050_PWR_MGMT_1,0x01);
	Write_MPU6050_Register(MPU6050_PWR_MGMT_1,0x00);
	Write_MPU6050_Register(MPU6050_SMPLRT_DIV,0x09);
	Write_MPU6050_Register(MPU6050_CONFIG,0x06);
	Write_MPU6050_Register(MPU6050_GYRO_CONFIG,0x18);
	Write_MPU6050_Register(MPU6050_ACCEL_CONFIG,0x18);
}

void Get_MPU6050_Data(int16_t *AccX ,int16_t *AccY ,int16_t *AccZ ,
	                    int16_t *GyroX ,int16_t *GyroY ,int16_t *GyroZ)
{
	uint8_t DataH,DataL;
	DataH = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_H);
	DataL = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_L);
	*AccX = (DataH << 8) | DataL;
	
	DataH = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_H);
	DataL = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_L);
	*AccY = (DataH << 8) | DataL;
	
	DataH = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_H);
	DataL = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_L);
	*AccZ = (DataH << 8) | DataL;
	
	DataH = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_H);
	DataL = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_L);
	*GyroX  = (DataH << 8) | DataL;
	
	DataH = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_H);
	DataL = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_L);
	*GyroY  = (DataH << 8) | DataL;
	
	DataH = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_H);
	DataL = Read_MPU6050_Register(MPU6050_ACCEL_XOUT_L);
	*GyroZ  = (DataH << 8) | DataL;
	
}
